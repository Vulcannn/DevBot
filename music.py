import discord
from discord.ext import commands

class Music:
    """All Music Bot Commands"""

    def __init__(self, bot):
        self.bot = bot


    @commands.command(name='play') 
    async def play():
        """Play Command"""
        await bot.say('')

    @commands.command(name='np') 
    async def np():
        """Display the current song that is playing"""
        await bot.say('')

    @commands.command(name='skip') 
    async def skip():
        """Skip the current song"""
        await bot.say('')

def setup(bot):
    bot.add_cog(Music(bot))

