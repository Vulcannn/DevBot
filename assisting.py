import discord
import asyncio
from discord.ext import commands
client = discord.Client()



class Assisting:
    """Dev Bot commands to make your life easier"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def getroleid(cmd, args, message, app):
        for role in message.server.roles:
            if role.name == args[0]:
                    await self.bot.message(message.channel, f(role.id))


def setup(bot):
    bot.add_cog(Assisting(bot))