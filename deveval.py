from discord.ext import commands
from extras import permchecks
import discord
import inspect


import datetime
from collections import Counter

class Deveval:
    """Debug stuff/eval"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, hidden=False)
    @permchecks.is_owner()
    async def pyeval(self, ctx, *, code : str):
        """Python eval command, owner only"""
        code = code.strip('` ')
        python = '```py\n{}\n```'
        result = None

        env = {
            'bot': self.bot,
            'ctx': ctx,
            'message': ctx.message,
            'server': ctx.message.server,
            'channel': ctx.message.channel,
            'author': ctx.message.author
        }

        env.update(globals())

        try:
            result = eval(code, env)
            if inspect.isawaitable(result):
                result = await result
        except Exception as e:
            await self.bot.say(python.format(type(e).__name__ + ': ' + str(e)))
            return

        await self.bot.say(python.format(result))

def setup(bot):
    bot.add_cog(Deveval(bot))