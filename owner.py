import discord
from discord.ext import commands
import os

client = discord.Client()

class Owner:
    """Owner commands for Dev Bot"""

    def __init__(self, bot):
        self.bot = bot

@commands.command(pass_context=True)
async def on_eval(message, args):
	try:
		result = eval(args)
		await client.send_message(message.channel, result)
	except Exception as error:
		await client.send_message(message.channel, error)



def setup(bot):
    bot.add_cog(Owner(bot))