import discord
from discord.ext import commands
from .utils import checks  

class Moderation:
    """General Moderation commands."""

    def __init__(self, bot):
        self.bot = bot


    @commands.command(no_pm=True, pass_context=True)
    @permchecks.admin_or_permissions(kick_members=True)
    async def kick(self, ctx, user: discord.Member, *, reason: str = None):
        """Kicks user."""
        author = ctx.message.author
        server = author.server

        if author == user:
            await self.bot.say("I cannot let you do that. Self-harm is "
                               "bad \N{PENSIVE FACE}")
            return
        elif not self.is_allowed_by_hierarchy(server, author, user):
            await self.bot.say("I cannot let you do that. You are "
                               "not higher than the user in the role "
                               "hierarchy.")
            return

        try:
            await self.bot.kick(user)
            logger.info("{}({}) kicked {}({})".format(
                author.name, author.id, user.name, user.id))
            await self.new_case(server,
                                action="KICK",
                                mod=author,
                                user=user,
                                reason=reason)
            await self.bot.say("User was kicked :ok_hand:")
        except discord.errors.Forbidden:
            await self.bot.say("Sorry, but I cannot do that for you.")
        except Exception as e:
            print(e)

def setup(bot):
    bot.add_cog(Moderation(bot))
